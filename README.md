# TP analyse d image

Les TP seront l'occasion de developper des algorithmes en python (jupyter Notebooks) exploitant la librairie OpenCV

#A télécharger et installer sur vos machines avant les TP

Ma Python version = 3.6.10 |Anaconda, Inc.| (default, May 7 2020, 19:46:08) [MSC v.1916 64 bit (AMD64)]
sys.version_info(major=3, minor=6, micro=10, releaselevel='final', serial=0)
OPENCV Version = 3.4.2

#Voir ci-dessous les autres ressources à telecharger pour realiser les TPs

Ressources pour le development d'un script "image tagging" (Partie 4 => séances 3 et 4)

    Lien vers des bases d'images test : COCO : http://cocodataset.org/#home - autres : http://deeplearning.net/datasets/
    Format des résultats à produire : 1 image = 1 fichier txt contenant une liste de mots-clé séparés par des virgules (nom du fichier = nom de     l'image - ex image1.txt)
    Info sur le programme Python ImgTagging fourni. Il permet :

    de visualiser les mots-clés (stockés dans le sous-repertoire ./GT) associés à des images situées dans le répertoire choisi au démarrage
    d'évaluer les performances de votre algorithmes (plugins ImageJ) => Bouton [GT] en comparant les mots clés contenus dans vos fichiers Txt (situés dans meme repertoire que les images) avec les mots-clés contenus dans les fichiers Txt situés dans le repertoire ./GT
    Creation d'un fichier LOG.TXT dans le répertoire ./GT contenant les résultats (nb mots clés manquants, nb mots-clés en trop)


Liste des mot-clés à associer aux images : 

    Color : yellow, green, red, blue, brown, gray, white, black, orange
    Scene = Indoor – outdoor – countryside – city
    Content inside = human – face – animal – car – plane – van – byke 
    Content outside = sky – sun – house – tree – water - sea
    Quality = Fuzzy, sharp, dark, light 
    Complexity:= solated, multiple


#Chaque monome / binome doit remettre un compte-rendu de TP sous forme d'un fichier ZIP (précisant les noms du binome) contenant :

- un unique document PDF (ou eventuellement un jupyter Notebook) contenant toutes les réponses de tous les exercices de chacune des séances 
- le code Python (largement expliqué et commenté) d'Image Tagging developpé durant les dernière seances.
- Date limite de remise des Compte-rendus : 14 01 2022 avant 23h

## Authors and acknowledgment
Alexis CASSAGNAUD
Abdoullah EL FAHIM

## License
Open source projects, ce projet est un logiciel libre : vous pouvez le redistribuer et/ou le modifier selon les termes de la licence publique generale amoindrie GNU telle que publiee par la Free Software Foundation, soit la version 3 de la licence ou (a votre choix) toute version ulterieure.

